//
//  ApiManager.swift
//  NetworkingIOSTest
//
//  Created by Steewe MacBook Pro on 30/01/16.
//  Copyright © 2016 Steewe. All rights reserved.
//

import UIKit

public enum TaskError: ErrorType {
    case NetworkError(error:NSError?)
    case JSONError(error:NSError?)
}

typealias TasksHandler = (NSArray?, TaskError?) -> Void


let API_URL = "https://bitbucket.org/"
let PATH = "fatunicorn/iostest/raw/78f902ddb31ac96bb23b901054442dee52664271/data/tasklist.json"

let _sharedAPIManager = ApiManager(baseURL: NSURL(string: API_URL)!)

class ApiManager: AFHTTPSessionManager {

    class var sharedInstance : ApiManager {
        return _sharedAPIManager
    }
    
    init(baseURL url: NSURL!) {
        let conf = NSURLSessionConfiguration.defaultSessionConfiguration()
        super.init(baseURL: url, sessionConfiguration: conf)
        self.responseSerializer = AFHTTPResponseSerializer()//AFJSONResponseSerializer()
        self.requestSerializer = AFHTTPRequestSerializer()
        self.requestSerializer.HTTPShouldHandleCookies = false
        self.requestSerializer.timeoutInterval=60
        self.securityPolicy.allowInvalidCertificates = true
        self.securityPolicy.validatesDomainName = false
        AFNetworkReachabilityManager.sharedManager().startMonitoring()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    /// Check whether the device is connected to the internet.

    func connected() -> Bool {
        return AFNetworkReachabilityManager.sharedManager().reachable
    }
    

    /// Check if the device is connected to the internet, if not show an alert.

    static func checkConnection() {
        if !self.sharedInstance.connected() {
            UIAlertView(title: "No Network", message: "There is no active network connection, please connect to a working network and try again.", delegate: nil, cancelButtonTitle: "OK").show()
        }
    }
    
    /// Get a list of Tasks and save in Context
    ///
    /// - completion: the block which will be executed after the api call was called. The parameters of the block are the following:

    static func getTasks(resultCompletion: TasksHandler?) {

        self.sharedInstance.GET(PATH,
            parameters: nil,
            progress: nil,
            success: { (session : NSURLSessionDataTask, response) -> Void in
                do{
                    let json =  try NSJSONSerialization.JSONObjectWithData(response as! NSData, options: .MutableContainers) as? NSArray
                    var objIds = [NSManagedObjectID]()
                    MagicalRecord.saveWithBlock({ (context) -> Void in
                        let tasks = Task.tasksFromArray(json!, context: context)
                        for object in tasks {
                            objIds.append(object.objectID)
                        }
                    }, completion: { (success, error) -> Void in
                        resultCompletion?(Task.MR_findAll(), nil)
                    })

                }catch let error as NSError {
                    resultCompletion?(nil, TaskError.JSONError(error: error))
                }
                
            }) { (session, error) -> Void in
                self.checkConnection()
                resultCompletion?(nil, TaskError.NetworkError(error: error))

        }

    }
}
