//
//  CacheImage.swift
//  NetworkingIOSTest
//
//  Created by Steewe MacBook Pro on 30/01/16.
//  Copyright © 2016 Steewe. All rights reserved.
//

import UIKit

extension NSURL {
    
    typealias ImageCacheCompletion = UIImage -> Void
    
    var cachedImage: UIImage? {
        return CacheImage.sharedCache.objectForKey(absoluteString) as? UIImage
    }

    
    /// Get image from URL and cacheing
    ///
    /// - completion: the block which will be executed the call. The parameters of the block are the following:
    ///     - image: UIImage result.

    
    func fetchImage(completion: ImageCacheCompletion) {
        let task = NSURLSession.sharedSession().dataTaskWithURL(self) {
            data, response, error in
            if error == nil {
                if let  data = data,
                    image = UIImage(data: data) {
                        CacheImage.sharedCache.setObject(
                            image,
                            forKey: self.absoluteString,
                            cost: data.length)
                        dispatch_async(dispatch_get_main_queue()) {
                            completion(image)
                        }
                }
            }
        }
        task.resume()
    }
    
}

class CacheImage {

    static let sharedCache: NSCache = {
        let cache = NSCache()
        cache.name = "CacheImage"
        cache.countLimit = 20
        cache.totalCostLimit = 10*1024*1024
        return cache
    }()
    
}
