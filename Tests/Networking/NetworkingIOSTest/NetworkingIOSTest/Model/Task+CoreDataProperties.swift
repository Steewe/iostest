//
//  Task+CoreDataProperties.swift
//  
//
//  Created by Steewe MacBook Pro on 30/01/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Task {

    @NSManaged var done: NSNumber?
    @NSManaged var image: String?
    @NSManaged var taskTitle: String?
    @NSManaged var taskId: String?

}
