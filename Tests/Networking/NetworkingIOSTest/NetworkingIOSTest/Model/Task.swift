//
//  Task.swift
//  
//
//  Created by Steewe MacBook Pro on 30/01/16.
//
//

import Foundation
import CoreData


class Task: NSManagedObject {

    static func taskFromDictionary(dict: NSDictionary, context: NSManagedObjectContext!) -> Task {

        let task = Task.MR_findFirstOrCreateByAttribute("taskId", withValue: dict["task"]!, inContext: context)
        
        if let taskTitle = dict["task"] as? String {
            task.taskTitle = taskTitle
        }
        
        if let image = dict["image"] as? String {
            task.image = image
        }
        
        if let done = dict["done"] as? NSNumber {
            task.done = done
        }
        return task
    }
    
    static func tasksFromArray(array: NSArray, context: NSManagedObjectContext) -> [Task] {
        var result = [Task]()
        for task in array {
            result.append(Task.taskFromDictionary(task as! NSDictionary, context: context))
        }
        
        return result
    }


}
