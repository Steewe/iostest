//
//  DetailViewController.swift
//  NetworkingIOSTest
//
//  Created by Steewe MacBook Pro on 30/01/16.
//  Copyright © 2016 Steewe. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    var task = Task?()
    let imageView = UIImageView()

    convenience init(task: Task?) {
        self.init()
        if let _ = task { self.task=task! }
        
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.task?.taskTitle
        self.view.backgroundColor = .whiteColor()
        
        self.imageView.translatesAutoresizingMaskIntoConstraints = false
        self.imageView.contentMode = .ScaleAspectFit
        self.view.addSubview(self.imageView)
        
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[image]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["image":self.imageView]))
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[image]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["image":self.imageView]))
        
        let imageUrl = NSURL(string:(self.task!.image!))
        
        if let image = imageUrl?.cachedImage {
            self.imageView.image = image
        } else {
            imageUrl?.fetchImage { image in
                self.imageView.image=image
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
