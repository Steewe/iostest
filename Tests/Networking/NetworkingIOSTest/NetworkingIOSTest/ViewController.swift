//
//  ViewController.swift
//  NetworkingIOSTest
//
//  Created by Steewe MacBook Pro on 30/01/16.
//  Copyright © 2016 Steewe. All rights reserved.
//

import UIKit

let CellIdentifier = "Cell"
let HeaderIdentifier = "Header"
let SizeCell:CGFloat = 60.0

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    let segmentControl = UISegmentedControl(items: ["ToDo","Done","All"])
    let tableView = UITableView(frame: CGRectZero, style: .Plain)
    var taskList = [Task]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "List"
        self.view.backgroundColor = .blueColor()
        self.segmentControl.selectedSegmentIndex = 2
        self.createElements()
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.loadData()
    }

    

    /// loadData from ApiManager
    
    func loadData(){
        ApiManager.getTasks { (result, error) -> Void in
            if(result != nil){
                self.taskList=result as! [Task]
                self.tableView.reloadData()
            }else if(error != nil){
                
                switch (error!){
                   case let .NetworkError(error):
                        UIAlertView(title: "Network  Error", message: error?.description , delegate: nil, cancelButtonTitle: "OK").show()
                    break
                    
                    case let .JSONError(error):
                        UIAlertView(title: "Json  Error", message: error?.description , delegate: nil, cancelButtonTitle: "OK").show()
                    break
                }
                
                
            }
            
        }
    }
    
    func createElements(){
        self.segmentControl.translatesAutoresizingMaskIntoConstraints = false
        self.segmentControl.tintColor = .whiteColor()
        self.segmentControl.addTarget(self, action: Selector("segmentSelectAction"), forControlEvents: .ValueChanged)
        self.view.addSubview(self.segmentControl)
        
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: CellIdentifier)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        if #available(iOS 9.0, *) {
            tableView.cellLayoutMarginsFollowReadableWidth=false
        }
        
        
        self.view.addSubview(self.tableView)
        
        let views = ["segment":self.segmentControl,"table":self.tableView]
        
        self.view.addConstraints(NSLayoutConstraint .constraintsWithVisualFormat("H:|[segment]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        self.view.addConstraints(NSLayoutConstraint .constraintsWithVisualFormat("H:|[table]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        self.view.addConstraints(NSLayoutConstraint .constraintsWithVisualFormat("V:|[segment(44)][table]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
    }
    
    func segmentSelectAction(){
        self.tableView.reloadData()
    }
    
    /// Get tasks and save in Context
    /// - parameters:
    ///   - indexPath: An index path that locates a row in tableView
    ///
    /// - returns: A Task
    
    func getTaskFromIndexPath(indexPath:NSIndexPath) -> Task {
        switch(self.segmentControl.selectedSegmentIndex){
        case 0:
            return self.taskList.filter{$0.done == false}[indexPath.row]
        case 1:
            return self.taskList.filter{$0.done == true}[indexPath.row]
        default:
            return self.taskList[indexPath.row]
        }

    }
    
    //MARK: - TableView Delegate Methods
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath){
        
        // iOS 8
        if(tableView.respondsToSelector(Selector("setLayoutMargins:"))){
            tableView.separatorInset = UIEdgeInsetsZero
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return SizeCell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch(self.segmentControl.selectedSegmentIndex){
        case 0:
            return self.taskList.filter{$0.done == false}.count
        case 1:
            return self.taskList.filter{$0.done == true}.count
        default:
            return self.taskList.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let myCell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier, forIndexPath: indexPath)
        
        myCell.textLabel?.text = self.getTaskFromIndexPath(indexPath).taskTitle
        myCell.imageView?.image = nil
        myCell.imageView?.contentMode = .ScaleAspectFit
        
        let imageUrl = NSURL(string:(self.getTaskFromIndexPath(indexPath).image!))!
        
        if let image = imageUrl.cachedImage {
            myCell.imageView?.image = image
        } else {
            imageUrl.fetchImage { image in
                if let cellToUpdate = tableView.cellForRowAtIndexPath(indexPath) {
                    cellToUpdate.imageView?.image = image
                    tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .None)
                }
                
            }
        }
        
        return myCell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let detail = DetailViewController(task: self.getTaskFromIndexPath(indexPath))
        self.navigationController?.pushViewController(detail, animated: true)
    }
}

