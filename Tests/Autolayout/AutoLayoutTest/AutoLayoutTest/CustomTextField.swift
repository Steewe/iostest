//
//  CustomTextField.swift
//  AutoLayoutTest
//
//  Created by Steewe MacBook Pro on 30/01/16.
//  Copyright © 2016 Steewe. All rights reserved.
//

import UIKit

class CustomTextField: UITextField, UITextFieldDelegate {

    
    var layerBorder:CAShapeLayer=CAShapeLayer()
    weak var delegateController:ViewControllerDelegate?
    
    override init (frame : CGRect) {
        super.init(frame : frame)
        self.initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
    func initView(){
        self.delegate=self
        self.returnKeyType = .Default
        self.textColor = UIColor.blackColor()
        self.layer.backgroundColor=UIColor.whiteColor().CGColor
        self.layerBorder.fillColor=UIColor.clearColor().CGColor
        self.layerBorder.strokeColor=UIColor.lightGrayColor().CGColor
        self.layerBorder.lineWidth=2.0
        self.layer.addSublayer(self.layerBorder)
        
        let padding = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: 10, height: 0)))
        self.leftView=padding
        self.rightView=padding
        self.leftViewMode=UITextFieldViewMode.Always
        self.rightViewMode=UITextFieldViewMode.Always
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let maskPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 10)
        let maskLayer = CAShapeLayer()
        maskLayer.path=maskPath.CGPath
        maskLayer.fillColor=UIColor.redColor().CGColor
        self.layer.mask=maskLayer
        
        self.layerBorder.path=maskPath.CGPath
        let newRect = CGRectMake(10, self.bounds.origin.y, self.bounds.size.width-20, self.bounds.size.height)
        self.placeholderRectForBounds(newRect)
        
        
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        self.delegateController?.textFieldSelected=self
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
