//
//  ViewController.swift
//  AutoLayoutTest
//
//  Created by Steewe MacBook Pro on 30/01/16.
//  Copyright © 2016 Steewe. All rights reserved.
//

import UIKit

protocol ViewControllerDelegate: class{
    var textFieldSelected:CustomTextField? { get set }
}

let MARGIN_OFFSET:CGFloat = 30

class ViewController: UIViewController, ViewControllerDelegate {

    let firstTextField = CustomTextField()
    let secondTextField = CustomTextField()
    let button = UIButton(type: UIButtonType.System)
    let contentView = UIView()
    var constraintcenter:NSLayoutConstraint?
    var textFieldSelected:CustomTextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .redColor()
        
        self.createElements()
        
        let gestureTap = UITapGestureRecognizer(target:self , action: Selector("dismissKeyboard"))
        self.view.addGestureRecognizer(gestureTap)

    }
    
    override func viewWillAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func createElements(){
        
        //MARK: - Content Elements
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.contentView)
        
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[content]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["content":self.contentView]))
        //self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|->=0-[content]->=0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["content":self.contentView]))
        
        self.constraintcenter = NSLayoutConstraint(item: self.contentView, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.CenterY, multiplier: 1.0, constant: 0)
        
        self.view.addConstraint(self.constraintcenter!)
        
        //MARK: - First TextField
        self.firstTextField.placeholder = "Email"
        self.firstTextField.translatesAutoresizingMaskIntoConstraints = false
        self.firstTextField.delegateController = self
        self.contentView.addSubview(self.firstTextField)
        
        //MARK: - Second TextField
        self.secondTextField.placeholder = "Password"
        self.secondTextField.secureTextEntry = true
        self.secondTextField.translatesAutoresizingMaskIntoConstraints = false
        self.secondTextField.delegateController = self
        self.contentView.addSubview(self.secondTextField)
        
        //MARK: - Button
        self.button.setTitle("Login", forState: .Normal)
        self.button.setTitleColor(.whiteColor(), forState: .Normal)
        self.button.backgroundColor = .blueColor()
        self.button.layer.cornerRadius = 10
        self.button.clipsToBounds = true
        self.button.addTarget(self, action: Selector("dismissKeyboard"), forControlEvents: .TouchUpInside)
        self.button.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(self.button)
        
        let views = ["first":self.firstTextField,"second":self.secondTextField,"button":self.button]
        
        self.contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-20-[first]-20-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        self.contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-20-[second]-20-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        self.contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-20-[button]-20-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        self.contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[first(44)]-[second(44)]-[button(44)]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        
        
        
    }
    
    func dismissKeyboard(){
        self.view.endEditing(true)
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        self.dismissKeyboard()
    }
    
    

    //MARK: Keyoboard Events
    
    func keyboardWillShow(sender: NSNotification) {
        if let userInfo = sender.userInfo {
            if let keyboardHeight = userInfo[UIKeyboardFrameEndUserInfoKey]?.CGRectValue.size.height {
                let yPosKeyboard = self.view.frame.size.height-keyboardHeight
                let limitDown = self.contentView.frame.origin.y+(self.textFieldSelected?.frame.origin.y)!+(self.textFieldSelected?.frame.size.height)!
                let difference = limitDown-yPosKeyboard-self.constraintcenter!.constant+MARGIN_OFFSET
                if difference > 0 {
                    self.constraintcenter?.constant = -difference
                    UIView.animateWithDuration(0.25, animations: { () -> Void in
                        self.view.layoutIfNeeded()
                    })
                }
            }
        }
    }
    
    func keyboardWillHide(sender: NSNotification) {
        if let userInfo = sender.userInfo {
            if let _ = userInfo[UIKeyboardFrameEndUserInfoKey]?.CGRectValue.size.height {
                self.constraintcenter?.constant = 0.0
                UIView.animateWithDuration(0.25, animations: { () -> Void in self.view.layoutIfNeeded() })
            }
        }
    }
}

