//
//  ViewController.swift
//  AVTest
//
//  Created by Steewe MacBook Pro on 31/01/16.
//  Copyright © 2016 Steewe. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    let captureSession = AVCaptureSession()
    let stillImageOutput = AVCaptureStillImageOutput()
    var previewLayer:AVCaptureVideoPreviewLayer?
    var flash:UIView?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .blackColor()
        
        let devices = AVCaptureDevice.devices().filter{ $0.hasMediaType(AVMediaTypeVideo) && $0.position == AVCaptureDevicePosition.Front }
        if let captureDevice = devices.first as? AVCaptureDevice  {
            
            do{
                //MARK: - Capture Session
                try captureSession.addInput(AVCaptureDeviceInput(device: captureDevice))
                
                self.captureSession.sessionPreset = AVCaptureSessionPresetPhoto
                self.captureSession.startRunning()
                
                self.stillImageOutput.outputSettings = [AVVideoCodecKey:AVVideoCodecJPEG]
                if self.captureSession.canAddOutput(self.stillImageOutput) {
                    self.captureSession.addOutput(self.stillImageOutput)
                }
                
                
                //MARK: - Preview Layer
                self.previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)!
                self.previewLayer?.bounds = self.view.bounds
                self.previewLayer?.position = CGPointMake(self.view.bounds.midX, self.view.bounds.midY)
                self.previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
                
                //MARK: Camera
                let cameraPreview = UIView()
                cameraPreview.layer.addSublayer(self.previewLayer!)
                cameraPreview.translatesAutoresizingMaskIntoConstraints = false
                
                self.view.addSubview(cameraPreview)
                
                self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[capture]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["capture":cameraPreview]))
                self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[capture]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["capture":cameraPreview]))
                
                //MARK: - Flash
                self.flash = UIView()
                self.flash?.backgroundColor = .whiteColor()
                self.flash?.translatesAutoresizingMaskIntoConstraints = false
                self.flash?.alpha = 0
                cameraPreview.addSubview(self.flash!)
                
                cameraPreview.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[flash]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["flash":self.flash!]))
                cameraPreview.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[flash]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["flash":self.flash!]))
                
                //MARK: - Button
                let button = UIButton(type: UIButtonType.Custom)
                button.backgroundColor = .redColor()
                button.translatesAutoresizingMaskIntoConstraints = false
                button.layer.cornerRadius = 40
                button.addTarget(self, action: Selector("takePhoto:"), forControlEvents: .TouchUpInside)
                cameraPreview.addSubview(button)
                
                cameraPreview.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|->=0-[button(80)]-20-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["button":button]))
                cameraPreview.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|->=0-[button(80)]-20-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["button":button]))
                
            }catch let error as NSError{
                UIAlertView(title: "Error", message: error.description, delegate: nil, cancelButtonTitle: "OK").show()
            }
        }else{
            UIAlertView(title: "SIMULATOR", message: nil, delegate: nil, cancelButtonTitle: "OK").show()
        }
        
    }
    
    func takePhoto(sender: UIButton) {
        if let videoConnection = self.stillImageOutput.connectionWithMediaType(AVMediaTypeVideo) {
            
            self.flash?.alpha = 1
            
            UIView.animateWithDuration(0.3, animations: { () -> Void in
                self.flash?.alpha = 0
            })
            
            self.stillImageOutput.captureStillImageAsynchronouslyFromConnection(videoConnection) {
                (imageDataSampleBuffer, error) -> Void in
                let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer)
                UIImageWriteToSavedPhotosAlbum(UIImage(data: imageData)!, nil, nil, nil)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        let orientation = AVCaptureVideoOrientation(rawValue: UIDevice.currentDevice().orientation.rawValue)!
        self.previewLayer?.connection.videoOrientation = orientation
        self.previewLayer?.bounds = self.view.bounds
        self.previewLayer?.position = CGPointMake(view.bounds.midX, view.bounds.midY)
    }
    
}

